const Book = require('../models/book.model');

// Create and Save a new Book
exports.create = (req, res) => {

  console.log(req.body);
  // Validate request
  if (req.body.content) {
    return res.status(400).send({
      message: "Book content can not be empty"
    });
  }

  // Create a Book
  const book = new Book({
    title: req.body.title || "Untitled Book",
    author: req.body.author,
    description: req.body.description,
    price: req.body.price
  });

  book.save()
    .then(data => {
      res.status(201).json({
        code: 200,
        message: " Book add sucessfully ",
        data : data
      });
    })
    .catch(err => {
      res.status(500).send({
        message: err.message || "Some error occurred while creating the  book"
      });
    });
};

// Retrieve and return all Books from the database.
exports.findAll = (req, res) => {

  Book.find()
      .then(data => {
        res.status(200).json({
          code: 200,
          message: " Book retrieve sucessfully ",
          data: data
        });
      })
      .catch(err => {
        res.status(500).send({
          message: err.message || "Can not retrive book's"
        });
      })

};

// Find a single Book with a noteId
exports.findOne = (req, res) => {

  const id = req.params.bookId;
  Book.findById(id)
    .then(book => {
      if(!book){
        return res.status(404).send({
          message: "Note not found with id " + id
        });    
      }

      res.status(200).json({
        code: 200,
        message: " Book retrieve sucessfully ",
        data: book
      });
    })
    .catch(err => {
      if (err.kind === 'ObjectId') {
        return res.status(404).send({
          message: "Note not found with id " + id
        });
      }
      res.status(500).send({
        message: err.message || "Can not retrive this book's" + id
      });
    });

};

// Update a Book identified by the BookId in the request
exports.update = (req, res) => {
  const id = req.params.bookId;
  Book.findByIdAndUpdate(id, {
    title:req.body.title,
    author: req.body.author,
    description: req.body.description,
    price : req.body.price
  }, { new : true})
  .then(book =>{

    if (!book) {
      return res.status(404).send({
        message: "Note not found with id " + id
      });
    }

    res.status(203).json({
      code: 200,
      message: " Book update sucessfully ",
      data: book
    });

  })
  .catch( err =>{
    if (err.kind === 'ObjectId') {
      return res.status(404).send({
        message: "Note not found with id " + id
      });
    }
    res.status(500).send({
      message: err.message || "Can not retrive this book's" + id
    });
  });

};

// Delete a Book with the specified BookId in the request
exports.deleteOne = (req, res) => {
  const id = req.params.bookId;
  Book.findByIdAndDelete(id)
    .then(book => {
      if (!book) {
        return res.status(404).send({
          message: "Note not found with id " + id
        });
      }

      res.status(200).json({
        code: 200,
        message: " Book delete sucessfully ",
        data: book
      });
    })
    .catch(err => {
      if (err.kind === 'ObjectId' || err.name === 'NotFound') {
        return res.status(404).send({
          message: "Note not found with id " + id
        });
      }
      res.status(500).send({
        message: err.message || "Could not delete this book's" + id
      });
    });

};

// Delete all Books 
exports.delete = (req, res) => {

};