module.exports = (app) => {

  const books = require('../controllers/book.controller');

  //create a new Book
  app.post('/book', books.create);

  // Retrieve all Books
  app.get('/books', books.findAll);

  // Retrieve a single book with bookId
  app.get('/book/:bookId', books.findOne);

  // Update a Book with bookId
  app.put('/book/:bookId', books.update);

  // Delete a Book with noteId
  app.delete('/book/:bookId', books.deleteOne);

  //Delete a All Books
  app.delete('/books', books.delete);
}