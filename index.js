const express = require('express');
const bodyParser = require('body-parser');

//create your app 
const app = express();

// parse request of content-type to application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended:true}));

//parse reqest of content-type to application/json
app.use(bodyParser.json());
app.use(express.json());

//configure databe 
const dbConfig = require('./config/database.config');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;

//connecting to database
mongoose.connect(dbConfig.url,{
  useNewUrlParser:true
}).then(()=>{
  console.log("Successfully connected to database");
}).catch(err =>{
  console.error(" Could not connect to database. Exiting now ...",err);
  process.exit();
});


//define a test route
app.get('/', (req, res)=>{
  res.json({
    'message': "Welcome to our Book Store API"
  });
});

// Require Books routes
require('./app/routes/book.routes')(app);

app.listen(3000, () =>{
  console.log( "Serve is listening on port 3000");
});